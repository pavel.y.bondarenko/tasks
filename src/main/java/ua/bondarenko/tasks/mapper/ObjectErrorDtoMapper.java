package ua.bondarenko.tasks.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import ua.bondarenko.tasks.dto.ErrorDto;
import ua.bondarenko.tasks.service.InternalizationService;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, imports = FieldError.class)
public abstract class ObjectErrorDtoMapper {

    protected InternalizationService internalizationService;

    @Autowired
    public void setInternalizationService(InternalizationService internalizationService) {
        this.internalizationService = internalizationService;
    }

    @Mapping(target = "field", expression = "java(((FieldError) objectError).getField())")
    @Mapping(target = "message", source = "objectError", qualifiedByName = "getMessage")
    public abstract ErrorDto errorToDto(ObjectError objectError);

    @Named("getMessage")
    protected String getMessageFromObjectError(ObjectError objectError) {
        return internalizationService.getMessage(objectError.getDefaultMessage());
    }
}

