package ua.bondarenko.tasks.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.springframework.data.domain.Page;
import ua.bondarenko.tasks.dto.AllTasksDto;
import ua.bondarenko.tasks.dto.AllTasksWithOwnersDto;
import ua.bondarenko.tasks.entity.TaskEntity;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = {TaskMapper.class, UserMapper.class})
public interface AllTasksMapper {

    @Mapping(target = "currentPage", source = "page.pageable.pageNumber")
    @Mapping(target = "numberOfPages", expression = "java(page.getTotalPages())")
    @Mapping(target = "tasks", source = "page.content")
    AllTasksDto pageToDto(Page<TaskEntity> page);

    @Mapping(target = "currentPage", source = "page.pageable.pageNumber")
    @Mapping(target = "numberOfPages", expression = "java(page.getTotalPages())")
    @Mapping(target = "tasks", source = "page.content")
    AllTasksWithOwnersDto pageToDtoWithOwners(Page<TaskEntity> page);
}
