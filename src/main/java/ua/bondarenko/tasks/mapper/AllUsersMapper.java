package ua.bondarenko.tasks.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.springframework.data.domain.Page;
import ua.bondarenko.tasks.dto.AllUsersDto;
import ua.bondarenko.tasks.entity.UserEntity;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = UserMapper.class)
public interface AllUsersMapper {

    @Mapping(target = "currentPage", source = "page.pageable.pageNumber")
    @Mapping(target = "numberOfPages", expression = "java(page.getTotalPages())")
    @Mapping(target = "users", source = "page.content")
    AllUsersDto pageToDto(Page<UserEntity> page);
}
