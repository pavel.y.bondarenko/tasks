package ua.bondarenko.tasks.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.bondarenko.tasks.dto.CreateUserDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.model.Role;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, imports = Role.class)
public abstract class UserMapper {
    protected PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public abstract UserDto toDto(UserEntity userEntity);

    @Mapping(target = "role", expression = "java(Role.ROLE_USER)")
    @Mapping(target = "password", expression = "java(passwordEncoder.encode(createUserDto.getPassword()))")
    public abstract UserEntity createDtoToEntity(CreateUserDto createUserDto);

    @Mapping(target = "password", expression = "java(passwordEncoder.encode(createUserDto.getPassword()))")
    public abstract void updateEntityFromCreateDto(CreateUserDto createUserDto, @MappingTarget UserEntity userEntity);
}
