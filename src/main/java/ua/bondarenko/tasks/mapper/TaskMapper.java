package ua.bondarenko.tasks.mapper;

import org.mapstruct.*;
import ua.bondarenko.tasks.dto.CreateTaskDto;
import ua.bondarenko.tasks.dto.TaskDto;
import ua.bondarenko.tasks.dto.TaskWithOwnerDto;
import ua.bondarenko.tasks.entity.TaskEntity;
import ua.bondarenko.tasks.model.Status;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING, uses = UserMapper.class, imports = Status.class)
public interface TaskMapper {

    @Mapping(target = "deadline", source = "deadline", dateFormat = "dd-MM-yyyy HH:mm")
    TaskDto toDto(TaskEntity taskEntity);

    @Mapping(target = "deadline", source = "deadline", dateFormat = "dd-MM-yyyy HH:mm")
    TaskWithOwnerDto toDtoWithOwner(TaskEntity taskEntity);

    @Mapping(target = "deadline", source = "deadline", dateFormat = "dd-MM-yyyy HH:mm")
    @Mapping(target = "status", expression = "java(Status.PLANNED)")
    TaskEntity createToEntity(CreateTaskDto createTaskDto);

    @Mapping(target = "deadline", source = "deadline", dateFormat = "dd-MM-yyyy HH:mm")
    void updateEntityFromCreateDto(CreateTaskDto createTaskDto, @MappingTarget TaskEntity taskEntity);
}
