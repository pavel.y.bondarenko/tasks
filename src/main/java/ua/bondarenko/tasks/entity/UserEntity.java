package ua.bondarenko.tasks.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.bondarenko.tasks.converter.RoleAttributeConverter;
import ua.bondarenko.tasks.model.Role;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private long id;
    private String name;
    private String login;
    private String password;

    @Convert(converter = RoleAttributeConverter.class)
    private Role role;

    @OneToMany(mappedBy = "owner")
    private Set<TaskEntity> taskEntities;
}
