package ua.bondarenko.tasks.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.bondarenko.tasks.converter.StatusAttributeConverter;
import ua.bondarenko.tasks.model.Status;

import java.time.LocalDateTime;

@Entity
@Table(name = "tasks")
@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private long id;
    private String description;
    private LocalDateTime deadline;

    @Convert(converter = StatusAttributeConverter.class)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity owner;
}