package ua.bondarenko.tasks.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@SecurityScheme(
        name = "basicAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
public class OpenAPIConfig {
    private static final String MY_LINKEDIN = "https://www.linkedin.com/in/pavlo-bondarenko-190086211/";

    @Value("${openapi.local-url}")
    private String localUrl;

    @Value("${openapi.global-url}")
    private String globalUrl;

    @Value("${project.version}")
    private String projectVersion;

    @Bean
    public OpenAPI myOpenAPI() {
        Server devServer = new Server();
        devServer.setUrl(globalUrl);
        devServer.setDescription("Global server URL!");

        Server prodServer = new Server();
        prodServer.setUrl(localUrl);
        prodServer.setDescription("Local server URL!");

        Contact contact = new Contact();
        contact.setEmail("pavel.y.bondarenko@gmail.com");
        contact.setName("Pavlo Bondarenko");
        contact.setUrl(MY_LINKEDIN);

        License mitLicense = new License()
                .name("MY License")
                .url(MY_LINKEDIN);

        Info info = new Info()
                .title("Task API")
                .version(projectVersion)
                .contact(contact)
                .description("This API exposes endpoints to manage users and their tasks.")
                .termsOfService(MY_LINKEDIN)
                .license(mitLicense);

        return new OpenAPI().info(info).servers(List.of(devServer, prodServer));
    }
}
