package ua.bondarenko.tasks.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ua.bondarenko.tasks.annotation.IsAdmin;
import ua.bondarenko.tasks.dto.*;
import ua.bondarenko.tasks.model.UserPrincipal;
import ua.bondarenko.tasks.service.TaskService;

@RestController
@RequestMapping("/tasks")
@Tag(name = "Tasks", description = "Tasks management endpoints!")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping
    @Operation(summary = "Get all personal tasks.",
            description = "Return page with all personal tasks. Number of page and number of tasks on page send in parameters.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = AllTasksDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public AllTasksDto getAll(@ParameterObject Pageable pageable) {
        return taskService.getAll(
                SecurityContextHolder.getContext().getAuthentication().getName(),
                pageable);
    }

    @IsAdmin
    @GetMapping("/full")
    @Operation(summary = "Get all tasks (for admin only).",
            description = "Return page with all tasks. Number of page and number of tasks on page send in parameters.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = AllTasksWithOwnersDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public AllTasksWithOwnersDto getAllWithOwner(@ParameterObject Pageable pageable) {
        return taskService.getAllWithOwner(pageable);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get personal task by id.",
            description = "Return personal task with id that send in path variable.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = TaskDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public TaskDto getById(@PathVariable(name = "id") long id) {
        return taskService.getById(
                SecurityContextHolder.getContext().getAuthentication().getName(),
                id);
    }

    @IsAdmin
    @GetMapping("/full/{id}")
    @Operation(summary = "Get task by id (for admin only).",
            description = "Return task with id that send in path variable.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = TaskWithOwnerDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public TaskWithOwnerDto getWithOwnerById(@PathVariable(name = "id") long id) {
        return taskService.getWithOwnerById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create task.",
            description = "Create task with data that send in body.",
            tags = "post",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "201",
            content = {@Content(schema = @Schema(implementation = TaskDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public TaskDto create(@RequestBody @Valid CreateTaskDto createTaskDto) {
        return taskService.create(
                SecurityContextHolder.getContext().getAuthentication().getName(),
                createTaskDto);
    }

    @IsAdmin
    @PatchMapping("/{id}")
    @Operation(summary = "Update task (for admin only).",
            description = "Update task data with data that send in body.",
            tags = "patch",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = TaskDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public TaskDto updateById(@PathVariable(name = "id") long id,
                              @RequestBody @Valid CreateTaskDto createTaskDto) {
        return taskService.updateById(id, createTaskDto);
    }

    @PatchMapping("/{id}/status")
    @Operation(summary = "Update task`s status.",
            description = "Update status of task with id that send in path variable with data that send in body.",
            tags = "patch",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = TaskDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public TaskDto updateStatusById(@PathVariable(name = "id") long id,
                                    @RequestBody StatusDto statusDto) {
        return taskService.updateStatusById(
                SecurityContextHolder.getContext().getAuthentication().getName(),
                id,
                statusDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete task (for admin only).",
            description = "Delete task with id that send in path variable.",
            tags = "delete",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public void deleteById(@PathVariable(name = "id") long id,
                           @AuthenticationPrincipal UserPrincipal userPrincipal) {
        taskService.deleteById(id, userPrincipal);
    }
}
