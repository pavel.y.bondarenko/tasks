package ua.bondarenko.tasks.controller;

import io.micrometer.core.annotation.Counted;
import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ua.bondarenko.tasks.annotation.IsAdmin;
import ua.bondarenko.tasks.dto.AllUsersDto;
import ua.bondarenko.tasks.dto.CreateUserDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.service.UserService;

@RestController
@RequestMapping("/users")
@Tag(name = "Users", description = "Users management endpoints!")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @IsAdmin
    @Timed("users.getAll")
    @Counted("users.getAll")
    @GetMapping
    @Operation(summary = "Get all users data (for admin only).",
            description = "Return page with all users data. Number of page and number of users on page send in parameters.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = AllUsersDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public AllUsersDto getAll(@ParameterObject Pageable pageable) {
        return userService.getAll(pageable);
    }

    @IsAdmin
    @GetMapping("/{id}")
    @Operation(summary = "Get user data by id (for admin only).",
            description = "Return user data with id that send in path variable.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = UserDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public UserDto getById(@PathVariable(name = "id") long id) {
        return userService.getById(id);
    }

    @GetMapping("/me")
    @Operation(summary = "Get information about yourself.",
            description = "Return your personal data.",
            tags = "get",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = UserDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public UserDto getMe() {
        return userService.getByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create user.",
            description = "Create user with data that send in body.",
            tags = "post")
    @ApiResponse(responseCode = "201",
            content = {@Content(schema = @Schema(implementation = UserDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public UserDto create(@RequestBody @Valid CreateUserDto createUserDto) {
        return userService.create(createUserDto);
    }

    @IsAdmin
    @PatchMapping("/{id}")
    @Operation(summary = "Update user data (for admin only).",
            description = "Update user data by id that send in path variable with data that send in body.",
            tags = "patch",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = UserDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public UserDto updateById(@PathVariable(name = "id") long id,
                              @RequestBody @Valid CreateUserDto createUserDto) {
        return userService.updateById(id, createUserDto);
    }

    @PatchMapping("/me")
    @Operation(summary = "Update personal user data.",
            description = "Update personal user data with data that send in body.",
            tags = "patch",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "200",
            content = {@Content(schema = @Schema(implementation = UserDto.class), mediaType = "application/json")})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public UserDto updateMe(@RequestBody @Valid CreateUserDto createUserDto) {
        return userService.updateByLogin(
                SecurityContextHolder.getContext().getAuthentication().getName(),
                createUserDto);
    }

    @IsAdmin
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete user by id (for admin only).",
            description = "Delete user with id that send in path variable.",
            tags = "delete",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})
    public void deleteById(@PathVariable(name = "id") long id) {
        userService.deleteById(id);
    }

    @DeleteMapping("/me")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete yourself.",
            description = "Delete personal data from server.",
            tags = "delete",
            security = @SecurityRequirement(name = "basicAuth"))
    @ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())})
    @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})
    public void deleteMe() {
        userService.deleteByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
    }
}
