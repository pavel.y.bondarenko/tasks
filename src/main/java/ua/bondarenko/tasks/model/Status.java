package ua.bondarenko.tasks.model;

import lombok.AccessLevel;
import lombok.Getter;

import java.util.Objects;
import java.util.Set;

@Getter
public enum Status {
    PLANNED("pl"),
    WORK_IN_PROGRESS("wo"),
    POSTPONED("po"),
    NOTIFIED("no"),
    SIGNED("si"),
    DONE("do"),
    CANCELLED("ca");

    static {
        PLANNED.validStatusIdsForTransition = Set.of(WORK_IN_PROGRESS, POSTPONED, CANCELLED);
        WORK_IN_PROGRESS.validStatusIdsForTransition = Set.of(POSTPONED, NOTIFIED, SIGNED, CANCELLED);
        POSTPONED.validStatusIdsForTransition = Set.of(WORK_IN_PROGRESS, NOTIFIED, SIGNED, CANCELLED);
        NOTIFIED.validStatusIdsForTransition = Set.of(SIGNED, DONE, CANCELLED);
        SIGNED.validStatusIdsForTransition = Set.of(DONE, CANCELLED);
    }

    private final String code;

    @Getter(AccessLevel.NONE)
    private Set<Status> validStatusIdsForTransition;

    Status(String code) {
        this.code = code;
    }

    public boolean canNotTransitTo(Status status) {
        return Objects.isNull(validStatusIdsForTransition) || !validStatusIdsForTransition.contains(status);
    }
}
