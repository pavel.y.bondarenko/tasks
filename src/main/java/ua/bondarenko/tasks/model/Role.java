package ua.bondarenko.tasks.model;

import lombok.Getter;

@Getter
public enum Role {
    ROLE_ADMIN("a"),
    ROLE_USER("u");

    private final String code;

    Role(String code) {
        this.code = code;
    }
}
