package ua.bondarenko.tasks.model;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorMessage {
    USER_LOGIN_NOT_FOUND(HttpStatus.UNAUTHORIZED, "user.login.not.found", 101),
    USER_LOGIN_EXISTS(HttpStatus.BAD_REQUEST, "user.login.exists", 102),
    USER_WITH_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "user.id.not.found", 103),

    TASK_WITH_ID_NOT_FOUND(HttpStatus.NOT_FOUND, "task.id.not.found", 201),
    CAN_NOT_CHANGE_STATUS(HttpStatus.BAD_REQUEST, "task.status.can.not.change", 202),

    DEADLINE_PAST(HttpStatus.BAD_REQUEST, "deadline.past", 301);

    private final HttpStatus status;
    private final String messageCode;
    private final int errorCode;

    ErrorMessage(HttpStatus status,
                 String messageCode,
                 int errorCode) {
        this.status = status;
        this.messageCode = messageCode;
        this.errorCode = errorCode;
    }
}