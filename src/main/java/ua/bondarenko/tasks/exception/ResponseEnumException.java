package ua.bondarenko.tasks.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import ua.bondarenko.tasks.model.ErrorMessage;

public class ResponseEnumException extends RuntimeException {

    private final ErrorMessage errorMessage;

    @Getter
    private final Object[] objects;

    public ResponseEnumException(ErrorMessage errorMessage,
                                 Object... objects) {
        this.errorMessage = errorMessage;
        this.objects = objects;
    }

    public int getErrorCode() {
        return errorMessage.getErrorCode();
    }

    public String getMessageCode() {
        return errorMessage.getMessageCode();
    }

    public HttpStatus getStatusCode() {
        return errorMessage.getStatus();
    }
}
