package ua.bondarenko.tasks.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ua.bondarenko.tasks.dto.ErrorDto;
import ua.bondarenko.tasks.dto.ResponseExceptionDto;
import ua.bondarenko.tasks.dto.ResponseValidationExceptionDto;
import ua.bondarenko.tasks.exception.ResponseEnumException;
import ua.bondarenko.tasks.mapper.ObjectErrorDtoMapper;
import ua.bondarenko.tasks.service.InternalizationService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class AppExceptionHandler extends ResponseEntityExceptionHandler {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    private final ObjectErrorDtoMapper objectErrorDtoMapper;
    private final InternalizationService internalizationService;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                  HttpHeaders headers,
                                                                  HttpStatusCode status,
                                                                  WebRequest request) {
        List<ErrorDto> errors = exception
                .getBindingResult()
                .getAllErrors()
                .stream()
                .map(objectErrorDtoMapper::errorToDto)
                .toList();

        ResponseValidationExceptionDto responseBody = createResponseValidationExceptionDto(
                HttpStatus.BAD_REQUEST.value(),
                internalizationService.getMessage("validation.failed"),
                1,
                errors
        );
        log.error("Validation failed - {}", errors, exception);
        return handleExceptionInternal(exception, responseBody, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(ResponseEnumException.class)
    public ResponseEntity<Object> handleResponseStatusException(ResponseEnumException exception,
                                                                WebRequest request) {
        ResponseExceptionDto responseBody = createResponseExceptionDto(
                exception.getStatusCode().value(),
                internalizationService.getMessage(exception.getMessageCode(), exception.getObjects()),
                exception.getErrorCode());

        log.error("{}", responseBody.getMessage(), exception);
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), exception.getStatusCode(), request);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException exception,
                                                              WebRequest request) {
        ResponseExceptionDto responseBody = createResponseExceptionDto(
                HttpStatus.NOT_FOUND.value(),
                internalizationService.getMessage("page.not.found"),
                HttpStatus.NOT_FOUND.value());

        log.error("Forbidden - {}", exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<Object> handleAccessDeniedException(DateTimeParseException exception,
                                                              WebRequest request) {
        ResponseExceptionDto responseBody = createResponseExceptionDto(
                HttpStatus.BAD_REQUEST.value(),
                internalizationService.getMessage("date.parse.exception", exception.getParsedString()),
                11);

        log.error("LocalDateTime parse exception - {}", exception.getMessage());
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllExceptions(Exception exception,
                                                      WebRequest request) {
        ResponseExceptionDto responseBody = createResponseExceptionDto(
                HttpStatus.BAD_REQUEST.value(),
                exception.getMessage(),
                666);
        log.error("WTF??? Unknown exception...", exception);
        return handleExceptionInternal(exception, responseBody, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    private ResponseExceptionDto createResponseExceptionDto(int status,
                                                            String message,
                                                            int code) {
        return new ResponseExceptionDto()
                .setCode(code)
                .setStatus(status)
                .setMessage(message)
                .setTimestamp(LocalDateTime.now().format(DATE_TIME_FORMATTER));
    }

    private ResponseValidationExceptionDto createResponseValidationExceptionDto(int status,
                                                                                String message,
                                                                                int code,
                                                                                List<ErrorDto> errors) {
        return new ResponseValidationExceptionDto()
                .setCode(code)
                .setStatus(status)
                .setMessage(message)
                .setTimestamp(LocalDateTime.now().format(DATE_TIME_FORMATTER))
                .setErrors(errors);
    }
}
