package ua.bondarenko.tasks.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CreateTaskDto {

    @NotBlank(message = "field.not.blank")
    private String description;

    @Pattern(regexp = "\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}", message = "date.format")
    private String deadline;
}
