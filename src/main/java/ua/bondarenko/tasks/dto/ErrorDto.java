package ua.bondarenko.tasks.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@ToString
public class ErrorDto {
    private String field;
    private String message;
}
