package ua.bondarenko.tasks.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
public class AllTasksDto {
    private int currentPage;
    private int numberOfPages;
    private List<TaskDto> tasks;
}
