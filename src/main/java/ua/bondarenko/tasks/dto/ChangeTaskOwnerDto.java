package ua.bondarenko.tasks.dto;

import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeTaskOwnerDto {

    @Min(value = 1, message = "owner.id.min")
    private long ownerId;
}
