package ua.bondarenko.tasks.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AllTasksWithOwnersDto {
    private int currentPage;
    private int numberOfPages;
    private List<TaskWithOwnerDto> tasks;
}
