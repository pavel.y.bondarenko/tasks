package ua.bondarenko.tasks.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class CreateUserDto {

    @NotBlank(message = "field.not.blank")
    private String name;

    @Size(min = 3, max = 20, message = "login.size")
    @Pattern(regexp = "[a-zA-Z0-9]*", message = "login.symbol.invalid")
    private String login;

    @NotBlank(message = "field.not.blank")
    private String password;
}
