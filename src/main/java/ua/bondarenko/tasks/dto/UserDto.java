package ua.bondarenko.tasks.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.bondarenko.tasks.model.Role;

@Getter
@Setter
@Accessors(chain = true)
public class UserDto {
    private long id;
    private String name;
    private String login;
    private Role role;
}
