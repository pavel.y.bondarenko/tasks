package ua.bondarenko.tasks.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class AllUsersDto {
    private int currentPage;
    private int numberOfPages;
    private List<UserDto> users;
}
