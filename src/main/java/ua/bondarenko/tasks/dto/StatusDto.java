package ua.bondarenko.tasks.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.bondarenko.tasks.model.Status;

@Getter
@Setter
@Accessors(chain = true)
public class StatusDto {
    private Status status;
}
