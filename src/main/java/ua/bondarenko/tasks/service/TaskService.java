package ua.bondarenko.tasks.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.bondarenko.tasks.dto.*;
import ua.bondarenko.tasks.entity.TaskEntity;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.exception.ResponseEnumException;
import ua.bondarenko.tasks.mapper.AllTasksMapper;
import ua.bondarenko.tasks.mapper.TaskMapper;
import ua.bondarenko.tasks.model.ErrorMessage;
import ua.bondarenko.tasks.model.Role;
import ua.bondarenko.tasks.model.UserPrincipal;
import ua.bondarenko.tasks.repository.TaskRepository;
import ua.bondarenko.tasks.repository.UserRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class TaskService {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final AllTasksMapper allTasksMapper;
    private final TaskMapper taskMapper;

    public AllTasksDto getAll(String login,
                              Pageable pageable) {
        return allTasksMapper.pageToDto(taskRepository.findAllByOwnerLogin(login, pageable));
    }

    public AllTasksWithOwnersDto getAllWithOwner(Pageable pageable) {
        return allTasksMapper.pageToDtoWithOwners(taskRepository.findAll(pageable));
    }

    public TaskDto getById(String login,
                           long id) {
        if (!taskRepository.existsByIdAndOwnerLogin(id, login)) {
            throw new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id);
        }

        return taskMapper
                .toDto(taskRepository
                        .findById(id)
                        .orElseThrow(() -> new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id)));
    }

    public TaskWithOwnerDto getWithOwnerById(long id) {
        return taskMapper
                .toDtoWithOwner(taskRepository
                        .findById(id)
                        .orElseThrow(() -> new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id)));
    }

    public TaskDto create(String login,
                          CreateTaskDto createTaskDto) {
        checkDeadline(createTaskDto.getDeadline());

        UserEntity userEntity = userRepository
                .findByLogin(login)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_LOGIN_NOT_FOUND, login));
        TaskEntity taskEntity = taskMapper.createToEntity(createTaskDto).setOwner(userEntity);

        return taskMapper.toDto(taskRepository.save(taskEntity));
    }

    public TaskDto updateById(long id,
                              CreateTaskDto createTaskDto) {
        checkDeadline(createTaskDto.getDeadline());
        TaskEntity taskEntity = taskRepository
                .findById(id)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id));
        taskMapper.updateEntityFromCreateDto(createTaskDto, taskEntity);
        return taskMapper.toDto(taskRepository.save(taskEntity));
    }

    public TaskDto updateStatusById(String login,
                                    long id,
                                    StatusDto statusDto) {
        if (!taskRepository.existsByIdAndOwnerLogin(id, login)) {
            throw new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id);
        }

        TaskEntity taskEntity = taskRepository
                .findById(id)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id));

        if (taskEntity.getStatus().equals(statusDto.getStatus())) {
            return taskMapper.toDto(taskEntity);
        }

        if (taskEntity.getStatus().canNotTransitTo(statusDto.getStatus())) {
            throw new ResponseEnumException(
                    ErrorMessage.CAN_NOT_CHANGE_STATUS,
                    taskEntity.getStatus(),
                    statusDto.getStatus());
        }

        return taskMapper.toDto(taskRepository.save(taskEntity.setStatus(statusDto.getStatus())));
    }

    public void deleteById(long id,
                           UserPrincipal userPrincipal) {
        if (!taskRepository.existsById(id)
                || (!taskRepository.existsByIdAndOwnerLogin(id, userPrincipal.getUsername())
                && userPrincipal.getRole().equals(Role.ROLE_USER))) {
            throw new ResponseEnumException(ErrorMessage.TASK_WITH_ID_NOT_FOUND, id);
        }

        taskRepository.deleteById(id);
    }

    private void checkDeadline(String deadline) {
        if (LocalDateTime.parse(deadline, DATE_TIME_FORMATTER).isBefore(LocalDateTime.now())) {
            throw new ResponseEnumException(ErrorMessage.DEADLINE_PAST, deadline);
        }
    }
}
