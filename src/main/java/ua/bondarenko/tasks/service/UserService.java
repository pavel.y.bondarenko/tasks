package ua.bondarenko.tasks.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.bondarenko.tasks.dto.AllUsersDto;
import ua.bondarenko.tasks.dto.CreateUserDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.exception.ResponseEnumException;
import ua.bondarenko.tasks.mapper.AllUsersMapper;
import ua.bondarenko.tasks.mapper.UserMapper;
import ua.bondarenko.tasks.model.ErrorMessage;
import ua.bondarenko.tasks.model.UserPrincipal;
import ua.bondarenko.tasks.repository.UserRepository;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final AllUsersMapper allUsersMapper;
    private final UserMapper userMapper;

    public AllUsersDto getAll(Pageable pageable) {
        return allUsersMapper.pageToDto(userRepository.findAll(pageable));
    }

    public UserDto getById(long id) {
        return userMapper
                .toDto(userRepository
                        .findById(id)
                        .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_WITH_ID_NOT_FOUND, id)));
    }

    public UserDto getByLogin(String login) {
        return userMapper
                .toDto(userRepository
                        .findByLogin(login)
                        .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_LOGIN_NOT_FOUND, login)));
    }

    public UserDto create(CreateUserDto createUserDto) {
        if (userRepository.existsByLogin(createUserDto.getLogin())) {
            throw new ResponseEnumException(ErrorMessage.USER_LOGIN_EXISTS, createUserDto.getLogin());
        }

        return userMapper.toDto(userRepository.save(userMapper.createDtoToEntity(createUserDto)));
    }

    public UserDto updateById(long id,
                              CreateUserDto createUserDto) {
        UserEntity userEntity = userRepository
                .findById(id)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_WITH_ID_NOT_FOUND, id));

        if (!userEntity.getLogin().equals(createUserDto.getLogin())
                && userRepository.existsByLogin(createUserDto.getLogin())) {
            throw new ResponseEnumException(ErrorMessage.USER_LOGIN_EXISTS, createUserDto.getLogin());
        }

        userMapper.updateEntityFromCreateDto(createUserDto, userEntity);
        return userMapper.toDto(userRepository.save(userEntity));
    }

    public UserDto updateByLogin(String login,
                                 CreateUserDto createUserDto) {
        UserEntity userEntity = userRepository
                .findByLogin(login)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_LOGIN_NOT_FOUND, login));

        if (!userEntity.getLogin().equals(createUserDto.getLogin())
                && userRepository.existsByLogin(createUserDto.getLogin())) {
            throw new ResponseEnumException(ErrorMessage.USER_LOGIN_EXISTS, createUserDto.getLogin());
        }

        userMapper.updateEntityFromCreateDto(createUserDto, userEntity);
        return userMapper.toDto(userRepository.save(userEntity));
    }

    public void deleteById(long id) {
        if (!userRepository.existsById(id)) {
            throw new ResponseEnumException(ErrorMessage.USER_WITH_ID_NOT_FOUND, id);
        }

        userRepository.deleteById(id);
    }

    public void deleteByLogin(String login) {
        if (!userRepository.existsByLogin(login)) {
            throw new ResponseEnumException(ErrorMessage.USER_LOGIN_NOT_FOUND, login);
        }

        userRepository.deleteByLogin(login);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository
                .findByLogin(username)
                .orElseThrow(() -> new ResponseEnumException(ErrorMessage.USER_LOGIN_NOT_FOUND, username));

        return new UserPrincipal(userEntity);
    }
}
