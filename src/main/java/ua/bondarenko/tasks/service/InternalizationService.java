package ua.bondarenko.tasks.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InternalizationService {
    private final MessageSource messageSource;

    public String getMessage(String messageCode,
                             Object... objects) {
        return messageSource.getMessage(messageCode, objects, LocaleContextHolder.getLocale());
    }
}
