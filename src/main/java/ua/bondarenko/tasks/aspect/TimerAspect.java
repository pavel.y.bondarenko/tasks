package ua.bondarenko.tasks.aspect;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
@Slf4j
public class TimerAspect {

    @Around("target(ua.bondarenko.tasks.controller.UserController)" +
            "|| target(ua.bondarenko.tasks.controller.TaskController)")
    public Object userControllerTimerExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch timer = new StopWatch();
        timer.start();
        Object result = joinPoint.proceed();
        timer.stop();
        log.debug(
                "{}, method {} execution time is {} millis",
                joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(),
                timer.getTime());
        return result;
    }
}
