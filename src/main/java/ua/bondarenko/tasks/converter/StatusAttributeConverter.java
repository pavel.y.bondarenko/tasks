package ua.bondarenko.tasks.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import ua.bondarenko.tasks.model.Status;

import java.util.stream.Stream;

@Converter
public class StatusAttributeConverter implements AttributeConverter<Status, String> {

    @Override
    public String convertToDatabaseColumn(Status status) {
        return status.getCode();
    }

    @Override
    public Status convertToEntityAttribute(String code) {
        return Stream.of(Status.values())
                .filter(status -> status.getCode().equals(code))
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid status in database!"));
    }
}
