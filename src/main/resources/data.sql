insert into users (name, login, password, role)
values ('Paul', 'admin', '$2a$12$sLMz9X/MCjVRymP2Jz7zh.6KCD2nAMWB.yYd.Cx3D/X0uz9s37X3K', 'a'),
       ('Ann', 'user', '$2a$12$a1ESmJoy5bRt6OByK/a6Oue0.FyqB7BFLFWSzi80N8RVc0RQ4vzQS', 'u');

insert into tasks (description, deadline, status, user_id)
values ('first admin''s task', {ts '2024-01-25 18:00:00.00'}, 'si', 1),
       ('second admin''s task', {ts '2024-01-26 15:00:00.00'}, 'wo', 1),
       ('third admin''s task', {ts '2024-01-27 16:00:00.00'}, 'pl', 1),
       ('first user''s task', {ts '2024-01-25 14:00:00.00'}, 'do', 2),
       ('second user''s task', {ts '2024-01-26 15:00:00.00'}, 'no', 2),
       ('third user''s task', {ts '2024-01-27 16:00:00.00'}, 'pl', 2);