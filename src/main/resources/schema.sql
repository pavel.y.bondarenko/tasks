create table users
(
    id       bigint       not null auto_increment,
    name     varchar(150) not null,
    login    varchar(150) not null unique,
    password varchar(100) not null,
    role     char(1)      not null,
    primary key (id)
);

create table tasks
(
    id          bigint       not null auto_increment,
    description varchar(500) not null,
    deadline    timestamp    not null,
    status      char(2)      not null,
    user_id     bigint references users (id) on delete cascade,
    primary key (id)
)
