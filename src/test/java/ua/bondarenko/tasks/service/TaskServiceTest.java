package ua.bondarenko.tasks.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bondarenko.tasks.dto.*;
import ua.bondarenko.tasks.entity.TaskEntity;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.exception.ResponseEnumException;
import ua.bondarenko.tasks.mapper.AllTasksMapper;
import ua.bondarenko.tasks.mapper.TaskMapper;
import ua.bondarenko.tasks.model.Role;
import ua.bondarenko.tasks.model.Status;
import ua.bondarenko.tasks.model.UserPrincipal;
import ua.bondarenko.tasks.repository.TaskRepository;
import ua.bondarenko.tasks.repository.UserRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @InjectMocks
    private TaskService taskService;

    @Mock
    private TaskRepository taskRepositoryMock;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private AllTasksMapper allTasksMapperMock;

    @Mock
    private TaskMapper taskMapperMock;

    @Mock
    private Pageable pageableMock;

    @Mock
    private Page<TaskEntity> taskEntityPageMock;

    private final TaskEntity taskEntity = new TaskEntity().setId(1);
    private final UserEntity userEntity = new UserEntity().setId(1).setLogin("login").setRole(Role.ROLE_USER);
    private final UserEntity adminEntity = new UserEntity().setId(2).setLogin("admin").setRole(Role.ROLE_ADMIN);
    private final TaskEntity taskEntity2 = new TaskEntity().setId(2);
    private final TaskDto taskDto = new TaskDto().setId(1);
    private final TaskDto taskDto2 = new TaskDto().setId(2);

    private final TaskWithOwnerDto taskWithOwnerDto = new TaskWithOwnerDto().setId(1);

    @Test
    void getAllTest() {
        String login = "login";
        AllTasksDto allTasksDto = new AllTasksDto();

        when(taskRepositoryMock.findAllByOwnerLogin(login, pageableMock)).thenReturn(taskEntityPageMock);
        when(allTasksMapperMock.pageToDto(taskEntityPageMock)).thenReturn(allTasksDto);

        assertEquals(allTasksDto, taskService.getAll(login, pageableMock));
    }

    @Test
    void getAllWithOwnerTest() {
        AllTasksWithOwnersDto allTasksWithOwnersDto = new AllTasksWithOwnersDto();

        when(taskRepositoryMock.findAll(pageableMock)).thenReturn(taskEntityPageMock);
        when(allTasksMapperMock.pageToDtoWithOwners(taskEntityPageMock)).thenReturn(allTasksWithOwnersDto);

        assertEquals(allTasksWithOwnersDto, taskService.getAllWithOwner(pageableMock));
    }

    @Test
    void getByIdTest() {
        String login = "login";
        long id = 1;

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskEntity));
        when(taskMapperMock.toDto(taskEntity)).thenReturn(taskDto);

        assertEquals(taskDto, taskService.getById(login, id));
    }

    @Test
    void getByIdNotOwnerTest() {
        String login = "login";
        long id = 1;

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> taskService.getById(login, id));
    }

    @Test
    void getByIdWithIdNotExistsTest() {
        String login = "login";
        long id = 1;

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> taskService.getById(login, id));
    }

    @Test
    void getWithOwnerByIdTest() {
        long id = 1;

        when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskEntity));
        when(taskMapperMock.toDtoWithOwner(taskEntity)).thenReturn(taskWithOwnerDto);

        assertEquals(taskWithOwnerDto, taskService.getWithOwnerById(id));
    }

    @Test
    void getWithOwnerByIdWithIdNotExistsTest() {
        long id = 1;

        when(taskRepositoryMock.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> taskService.getWithOwnerById(id));
    }

    @Test
    void createTest() {
        String deadline = "deadline";
        String login = "login";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.plusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(userEntity));
            when(taskMapperMock.createToEntity(createTaskDto)).thenReturn(taskEntity);
            when(taskRepositoryMock.save(taskEntity)).thenReturn(taskEntity2);
            when(taskMapperMock.toDto(taskEntity2)).thenReturn(taskDto2);

            assertEquals(taskDto2, taskService.create(login, createTaskDto));
        }
    }

    @Test
    void createWithPastDeadlineTest() {
        String deadline = "deadline";
        String login = "login";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.minusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            assertThrows(ResponseEnumException.class, () -> taskService.create(login, createTaskDto));
        }
    }

    @Test
    void createWithUserLoginNotExistsTest() {
        String deadline = "deadline";
        String login = "login";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.plusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.empty());

            assertThrows(ResponseEnumException.class, () -> taskService.create(login, createTaskDto));
        }
    }

    @Test
    void updateByIdTest() {
        long id = 1;
        String deadline = "deadline";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.plusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskEntity));
            when(taskRepositoryMock.save(taskEntity)).thenReturn(taskEntity2);
            when(taskMapperMock.toDto(taskEntity2)).thenReturn(taskDto2);

            assertEquals(taskDto2, taskService.updateById(id, createTaskDto));

            verify(taskMapperMock, times(1)).updateEntityFromCreateDto(createTaskDto, taskEntity);
        }
    }

    @Test
    void updateByIdWithPastDeadlineTest() {
        long id = 1;
        String deadline = "deadline";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.minusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            assertThrows(ResponseEnumException.class, () -> taskService.updateById(id, createTaskDto));
        }
    }

    @Test
    void updateByIdWithIdNotExistsTest() {
        long id = 1;
        String deadline = "deadline";
        CreateTaskDto createTaskDto = new CreateTaskDto().setDeadline(deadline);
        LocalDateTime now = LocalDateTime.now();

        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = mockStatic(LocalDateTime.class)) {
            localDateTimeMockedStatic
                    .when(() -> LocalDateTime.parse(eq(deadline), any(DateTimeFormatter.class)))
                    .thenReturn(now.plusDays(2));
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);

            when(taskRepositoryMock.findById(id)).thenReturn(Optional.empty());

            assertThrows(ResponseEnumException.class, () -> taskService.updateById(id, createTaskDto));
        }
    }

    @Test
    void updateStatusByIdTest() {
        long id = 1;
        String login = "login";
        StatusDto statusDto = new StatusDto().setStatus(Status.WORK_IN_PROGRESS);
        TaskEntity taskPlanned = new TaskEntity().setId(1).setStatus(Status.PLANNED);

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskPlanned));
        when(taskRepositoryMock.save(taskPlanned)).thenReturn(taskEntity);
        when(taskMapperMock.toDto(taskEntity)).thenReturn(taskDto);

        assertEquals(taskDto, taskService.updateStatusById(login, id, statusDto));
    }

    @Test
    void updateStatusByIdWithNotOwnerTest() {
        long id = 1;
        String login = "login";
        StatusDto statusDto = new StatusDto().setStatus(Status.WORK_IN_PROGRESS);

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> taskService.updateStatusById(login, id, statusDto));
    }

    @Test
    void updateStatusByIdWithIdNotExistsTest() {
        long id = 1;
        String login = "login";
        StatusDto statusDto = new StatusDto().setStatus(Status.WORK_IN_PROGRESS);

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> taskService.updateStatusById(login, id, statusDto));
    }

    @Test
    void updateStatusByIdWithStatusThatCanNotTransitTest() {
        long id = 1;
        String login = "login";
        StatusDto illegalStatusDto = new StatusDto().setStatus(Status.DONE);
        TaskEntity taskPlanned = new TaskEntity().setId(1).setStatus(Status.PLANNED);

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskPlanned));

        assertThrows(ResponseEnumException.class, () -> taskService.updateStatusById(login, id, illegalStatusDto));
    }

    @Test
    void updateStatusByIdWithSameStatusTest() {
        long id = 1;
        String login = "login";
        StatusDto sameStatusDto = new StatusDto().setStatus(Status.PLANNED);
        TaskEntity taskPlanned = new TaskEntity().setId(1).setStatus(Status.PLANNED);

        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, login)).thenReturn(true);
        when(taskRepositoryMock.findById(id)).thenReturn(Optional.of(taskPlanned));
        when(taskMapperMock.toDto(taskPlanned)).thenReturn(taskDto);

        assertEquals(taskDto, taskService.updateStatusById(login, id, sameStatusDto));

        verify(taskRepositoryMock, never()).save(any());
    }

    @Test
    void deleteByIdUserOwnerTest() {
        long id = 1;

        when(taskRepositoryMock.existsById(id)).thenReturn(true);
        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, userEntity.getLogin())).thenReturn(true);

        assertDoesNotThrow(() -> taskService.deleteById(id, new UserPrincipal(userEntity)));

        verify(taskRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    void deleteByIdAdminNotOwnerTest() {
        long id = 1;

        when(taskRepositoryMock.existsById(id)).thenReturn(true);
        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, adminEntity.getLogin())).thenReturn(false);

        assertDoesNotThrow(() -> taskService.deleteById(id, new UserPrincipal(adminEntity)));

        verify(taskRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    void deleteByIdWithIdNotExistsTest() {
        long id = 1;
        UserPrincipal principal = new UserPrincipal(userEntity);

        when(taskRepositoryMock.existsById(id)).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> taskService.deleteById(id, principal));

        verify(taskRepositoryMock, never()).deleteById(id);
    }

    @Test
    void deleteByIdWithUserNotOwnerTest() {
        long id = 1;
        UserPrincipal principal = new UserPrincipal(userEntity);

        when(taskRepositoryMock.existsById(id)).thenReturn(true);
        when(taskRepositoryMock.existsByIdAndOwnerLogin(id, userEntity.getLogin())).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> taskService.deleteById(id, principal));

        verify(taskRepositoryMock, never()).deleteById(id);
    }
}