package ua.bondarenko.tasks.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ua.bondarenko.tasks.dto.AllUsersDto;
import ua.bondarenko.tasks.dto.CreateUserDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.exception.ResponseEnumException;
import ua.bondarenko.tasks.mapper.AllUsersMapper;
import ua.bondarenko.tasks.mapper.UserMapper;
import ua.bondarenko.tasks.model.Role;
import ua.bondarenko.tasks.repository.UserRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepositoryMock;

    @Mock
    private AllUsersMapper allUsersMapperMock;

    @Mock
    private UserMapper userMapperMock;

    @Mock
    private Pageable pageableMock;

    @Mock
    private Page<UserEntity> userEntityPageMock;

    private final UserEntity userEntity1 = new UserEntity().setId(1);
    private final UserEntity userEntity2 = new UserEntity().setId(2);
    private final UserDto userDto1 = new UserDto().setId(1);
    private final UserDto userDto2 = new UserDto().setId(2);

    @Test
    void getAllTest() {
        AllUsersDto allUsersDto = new AllUsersDto().setCurrentPage(1).setNumberOfPages(2);

        when(userRepositoryMock.findAll(pageableMock)).thenReturn(userEntityPageMock);
        when(allUsersMapperMock.pageToDto(userEntityPageMock)).thenReturn(allUsersDto);

        assertEquals(allUsersDto, userService.getAll(pageableMock));
    }

    @Test
    void getByIdTest() {
        long id = 1;

        when(userRepositoryMock.findById(id)).thenReturn(Optional.of(userEntity1));
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.getById(id));
    }

    @Test
    void getByIdWithIdNotExistsTest() {
        long id = 1;

        when(userRepositoryMock.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> userService.getById(id));
    }

    @Test
    void getByLoginTest() {
        String login = "login";

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(userEntity1));
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.getByLogin(login));
    }

    @Test
    void getByLoginWithNotExistLoginTest() {
        String login = "login";

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> userService.getByLogin(login));
    }

    @Test
    void createTest() {
        String login = "login";

        CreateUserDto createUserDto = new CreateUserDto().setLogin(login);

        when(userRepositoryMock.existsByLogin(login)).thenReturn(false);
        when(userMapperMock.createDtoToEntity(createUserDto)).thenReturn(userEntity1);
        when(userRepositoryMock.save(userEntity1)).thenReturn(userEntity2);
        when(userMapperMock.toDto(userEntity2)).thenReturn(userDto2);

        assertEquals(userDto2, userService.create(createUserDto));
    }

    @Test
    void createWithLoginExistsTest() {
        String login = "login";

        CreateUserDto createUserDto = new CreateUserDto().setLogin(login);

        when(userRepositoryMock.existsByLogin(login)).thenReturn(true);

        assertThrows(ResponseEnumException.class, () -> userService.create(createUserDto));
    }

    @Test
    void updateByIdWithOtherLoginTest() {
        long id = 1;
        String createLogin = "login";
        String entityLogin = "other login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(createLogin);
        UserEntity user = new UserEntity().setLogin(entityLogin);

        when(userRepositoryMock.findById(id)).thenReturn(Optional.of(user));
        when(userRepositoryMock.existsByLogin(createLogin)).thenReturn(false);
        when(userRepositoryMock.save(user)).thenReturn(userEntity1);
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.updateById(id, createUserDto));

        verify(userMapperMock, times(1)).updateEntityFromCreateDto(createUserDto, user);
    }

    @Test
    void updateByIdWithSameLoginTest() {
        long id = 1;
        String login = "login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(login);
        UserEntity user = new UserEntity().setLogin(login);

        when(userRepositoryMock.findById(id)).thenReturn(Optional.of(user));
        when(userRepositoryMock.save(user)).thenReturn(userEntity1);
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.updateById(id, createUserDto));

        verify(userMapperMock, times(1)).updateEntityFromCreateDto(createUserDto, user);
    }

    @Test
    void updateByIdWithOtherLoginThatExistsTest() {
        long id = 1;
        String createLogin = "login";
        String entityLogin = "other login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(createLogin);
        UserEntity user = new UserEntity().setLogin(entityLogin);

        when(userRepositoryMock.findById(id)).thenReturn(Optional.of(user));
        when(userRepositoryMock.existsByLogin(createLogin)).thenReturn(true);

        assertThrows(ResponseEnumException.class, () -> userService.updateById(id, createUserDto));
    }

    @Test
    void updateByIdWithIdNotExistsTest() {
        long id = 1;
        String createLogin = "login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(createLogin);

        when(userRepositoryMock.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> userService.updateById(id, createUserDto));
    }

    @Test
    void updateByLoginTest() {
        String createLogin = "login";
        String login = "other login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(createLogin);
        UserEntity user = new UserEntity().setLogin(login);

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(user));
        when(userRepositoryMock.existsByLogin(createLogin)).thenReturn(false);
        when(userRepositoryMock.save(user)).thenReturn(userEntity1);
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.updateByLogin(login, createUserDto));

        verify(userMapperMock, times(1)).updateEntityFromCreateDto(createUserDto, user);
    }

    @Test
    void updateByLoginWithSameLoginTest() {
        String login = "login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(login);
        UserEntity user = new UserEntity().setLogin(login);

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(user));
        when(userRepositoryMock.save(user)).thenReturn(userEntity1);
        when(userMapperMock.toDto(userEntity1)).thenReturn(userDto1);

        assertEquals(userDto1, userService.updateByLogin(login, createUserDto));

        verify(userMapperMock, times(1)).updateEntityFromCreateDto(createUserDto, user);
    }

    @Test
    void updateByLoginWithOtherLoginThatExistsTest() {
        String createLogin = "login";
        String login = "other login";
        CreateUserDto createUserDto = new CreateUserDto().setLogin(createLogin);
        UserEntity user = new UserEntity().setLogin(login);

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(user));
        when(userRepositoryMock.existsByLogin(createLogin)).thenReturn(true);

        assertThrows(ResponseEnumException.class, () -> userService.updateByLogin(login, createUserDto));
    }

    @Test
    void deleteByIdTest() {
        long id = 1;

        when(userRepositoryMock.existsById(id)).thenReturn(true);

        assertDoesNotThrow(() -> userService.deleteById(id));

        verify(userRepositoryMock, times(1)).deleteById(id);
    }

    @Test
    void deleteByIdWithIdNotExistsTest() {
        long id = 1;

        when(userRepositoryMock.existsById(id)).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> userService.deleteById(id));

        verify(userRepositoryMock, never()).deleteById(id);
    }

    @Test
    void deleteByLoginTest() {
        String login = "login";

        when(userRepositoryMock.existsByLogin(login)).thenReturn(true);

        assertDoesNotThrow(() -> userService.deleteByLogin(login));

        verify(userRepositoryMock, times(1)).deleteByLogin(login);
    }

    @Test
    void deleteByLoginWithLoginNotExistsTest() {
        String login = "login";

        when(userRepositoryMock.existsByLogin(login)).thenReturn(false);

        assertThrows(ResponseEnumException.class, () -> userService.deleteByLogin(login));

        verify(userRepositoryMock, never()).deleteByLogin(login);
    }

    @Test
    void loadUserByUsernameTest() {
        String login = "login";
        String password = "password";
        Role role = Role.ROLE_ADMIN;
        UserEntity user = new UserEntity()
                .setLogin(login)
                .setPassword(password)
                .setRole(role);

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.of(user));

        UserDetails userDetails = userService.loadUserByUsername(login);

        assertEquals(login, userDetails.getUsername());
        assertEquals(password, userDetails.getPassword());
        assertEquals(Set.of(new SimpleGrantedAuthority(role.name())), userDetails.getAuthorities());
    }

    @Test
    void loadUserByUsernameWithUsernameNotExistsTest() {
        String login = "login";

        when(userRepositoryMock.findByLogin(login)).thenReturn(Optional.empty());

        assertThrows(ResponseEnumException.class, () -> userService.loadUserByUsername(login));
    }
}