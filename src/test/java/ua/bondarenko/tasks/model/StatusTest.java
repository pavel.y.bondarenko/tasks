package ua.bondarenko.tasks.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class StatusTest {

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"WORK_IN_PROGRESS", "POSTPONED", "CANCELLED"})
    void plannedCanTransitToTest(Status status) {
        assertFalse(Status.PLANNED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"PLANNED", "NOTIFIED", "SIGNED", "DONE"})
    void plannedCanNotTransitToTest(Status status) {
        assertTrue(Status.PLANNED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"POSTPONED", "NOTIFIED", "SIGNED", "CANCELLED"})
    void workInProgressCanTransitToTest(Status status) {
        assertFalse(Status.WORK_IN_PROGRESS.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"PLANNED", "WORK_IN_PROGRESS", "DONE"})
    void workInProgressCanNotTransitToTest(Status status) {
        assertTrue(Status.WORK_IN_PROGRESS.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"WORK_IN_PROGRESS", "NOTIFIED", "SIGNED", "CANCELLED"})
    void postponedCanTransitToTest(Status status) {
        assertFalse(Status.POSTPONED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"PLANNED", "POSTPONED", "DONE"})
    void postponedCanNotTransitToTest(Status status) {
        assertTrue(Status.POSTPONED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"SIGNED", "DONE", "CANCELLED"})
    void notifiedCanTransitToTest(Status status) {
        assertFalse(Status.NOTIFIED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"PLANNED", "WORK_IN_PROGRESS", "POSTPONED", "NOTIFIED"})
    void notifiedCanNotTransitToTest(Status status) {
        assertTrue(Status.NOTIFIED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"DONE", "CANCELLED"})
    void signedCanTransitToTest(Status status) {
        assertFalse(Status.SIGNED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class, names = {"PLANNED", "WORK_IN_PROGRESS", "POSTPONED", "NOTIFIED", "SIGNED"})
    void signedCanNotTransitToTest(Status status) {
        assertTrue(Status.SIGNED.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class)
    void doneCanNotTransitToTest(Status status) {
        assertTrue(Status.DONE.canNotTransitTo(status));
    }

    @ParameterizedTest
    @EnumSource(value = Status.class)
    void cancelledCanNotTransitToTest(Status status) {
        assertTrue(Status.CANCELLED.canNotTransitTo(status));
    }
}