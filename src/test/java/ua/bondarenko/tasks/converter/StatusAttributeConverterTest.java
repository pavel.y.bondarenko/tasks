package ua.bondarenko.tasks.converter;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.bondarenko.tasks.model.Status;

import static org.junit.jupiter.api.Assertions.*;

class StatusAttributeConverterTest {
    private StatusAttributeConverter statusAttributeConverter;

    @BeforeEach
    void setUp() {
        statusAttributeConverter = new StatusAttributeConverter();
    }

    @AfterEach
    void tearDown() {
        statusAttributeConverter = null;
    }

    @Test
    void convertToDatabaseColumnTest() {
        assertEquals("pl", statusAttributeConverter.convertToDatabaseColumn(Status.PLANNED));
        assertEquals("wo", statusAttributeConverter.convertToDatabaseColumn(Status.WORK_IN_PROGRESS));
        assertEquals("po", statusAttributeConverter.convertToDatabaseColumn(Status.POSTPONED));
        assertEquals("no", statusAttributeConverter.convertToDatabaseColumn(Status.NOTIFIED));
        assertEquals("si", statusAttributeConverter.convertToDatabaseColumn(Status.SIGNED));
        assertEquals("do", statusAttributeConverter.convertToDatabaseColumn(Status.DONE));
        assertEquals("ca", statusAttributeConverter.convertToDatabaseColumn(Status.CANCELLED));
    }

    @Test
    void convertToEntityAttributeTest() {
        assertEquals(Status.PLANNED, statusAttributeConverter.convertToEntityAttribute("pl"));
        assertEquals(Status.WORK_IN_PROGRESS, statusAttributeConverter.convertToEntityAttribute("wo"));
        assertEquals(Status.POSTPONED, statusAttributeConverter.convertToEntityAttribute("po"));
        assertEquals(Status.NOTIFIED, statusAttributeConverter.convertToEntityAttribute("no"));
        assertEquals(Status.SIGNED, statusAttributeConverter.convertToEntityAttribute("si"));
        assertEquals(Status.DONE, statusAttributeConverter.convertToEntityAttribute("do"));
        assertEquals(Status.CANCELLED, statusAttributeConverter.convertToEntityAttribute("ca"));
    }
}