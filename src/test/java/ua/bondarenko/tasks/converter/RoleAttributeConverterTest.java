package ua.bondarenko.tasks.converter;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.bondarenko.tasks.model.Role;

import static org.junit.jupiter.api.Assertions.*;

class RoleAttributeConverterTest {
    private RoleAttributeConverter roleAttributeConverter;

    @BeforeEach
    void setUp() {
        roleAttributeConverter = new RoleAttributeConverter();
    }

    @AfterEach
    void tearDown() {
        roleAttributeConverter = null;
    }

    @Test
    void convertToDatabaseColumnTest() {
        assertEquals("a", roleAttributeConverter.convertToDatabaseColumn(Role.ROLE_ADMIN));
        assertEquals("u", roleAttributeConverter.convertToDatabaseColumn(Role.ROLE_USER));
    }

    @Test
    void convertToEntityAttributeTest() {
        assertEquals(Role.ROLE_ADMIN, roleAttributeConverter.convertToEntityAttribute("a"));
        assertEquals(Role.ROLE_USER, roleAttributeConverter.convertToEntityAttribute("u"));
    }
}