package ua.bondarenko.tasks.mapper;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.bondarenko.tasks.dto.AllUsersDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.entity.UserEntity;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {BCryptPasswordEncoder.class, UserMapperImpl.class, AllUsersMapperImpl.class})
class AllUsersMapperTest {

    @Autowired
    private AllUsersMapper allUsersMapper;

    private final UserEntity userEntity1 = new UserEntity().setId(1);
    private final UserEntity userEntity2 = new UserEntity().setId(2);
    private final List<UserEntity> content = List.of(userEntity1, userEntity2);
    private final int pageSize = 3;
    private final int pageNumber = 1;
    private final long totalUsers = 9;
    private final Page<UserEntity> page = new PageImpl<>(
            content,
            Pageable.ofSize(pageSize).withPage(pageNumber),
            totalUsers);

    @Test
    void pageToDtoTest() {
        AllUsersDto dto = allUsersMapper.pageToDto(page);
        assertEquals(pageNumber, dto.getCurrentPage());
        assertEquals(totalUsers / pageSize, dto.getNumberOfPages());

        List<UserDto> userDtos = dto.getUsers();
        assertEquals(content.size(), userDtos.size());

        IntStream
                .range(0, content.size())
                .forEach(index -> assertEquals(content.get(index).getId(), userDtos.get(index).getId()));
    }
}