package ua.bondarenko.tasks.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import ua.bondarenko.tasks.dto.ErrorDto;
import ua.bondarenko.tasks.service.InternalizationService;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ObjectErrorDtoMapperTest {
    private final ObjectErrorDtoMapper objectErrorDtoMapper = Mappers.getMapper(ObjectErrorDtoMapper.class);

    @Mock
    private InternalizationService internalizationServiceMock;

    @BeforeEach
    void setUp() {
        objectErrorDtoMapper.setInternalizationService(internalizationServiceMock);
    }

    @Test
    void errorToDtoTest() {
        String fieldName = "field";
        String message = "message";
        String internalizedMessage = "internalized message";
        ObjectError error = new FieldError("object", fieldName, message);

        when(internalizationServiceMock.getMessage(message)).thenReturn(internalizedMessage);

        ErrorDto errorDto = objectErrorDtoMapper.errorToDto(error);

        assertEquals(internalizedMessage, errorDto.getMessage());
        assertEquals(fieldName, errorDto.getField());
    }
}