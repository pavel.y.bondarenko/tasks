package ua.bondarenko.tasks.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.test.util.ReflectionTestUtils;
import ua.bondarenko.tasks.dto.CreateTaskDto;
import ua.bondarenko.tasks.dto.TaskDto;
import ua.bondarenko.tasks.dto.TaskWithOwnerDto;
import ua.bondarenko.tasks.entity.TaskEntity;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.model.Role;
import ua.bondarenko.tasks.model.Status;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class TaskMapperTest {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private final TaskMapper taskMapper = Mappers.getMapper(TaskMapper.class);

    private final LocalDateTime localDateTime = LocalDateTime.now();
    private final UserEntity userEntity = new UserEntity()
            .setId(1)
            .setName("name")
            .setLogin("login")
            .setPassword("password")
            .setRole(Role.ROLE_ADMIN);

    private final TaskEntity taskEntity = new TaskEntity()
            .setId(1)
            .setDeadline(localDateTime)
            .setDescription("description")
            .setStatus(Status.PLANNED)
            .setOwner(userEntity);

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(taskMapper, "userMapper", userMapper);
    }

    @Test
    void toDtoTest() {
        TaskDto taskDto = taskMapper.toDto(taskEntity);

        assertEquals(taskEntity.getId(), taskDto.getId());
        assertEquals(taskEntity.getStatus(), taskDto.getStatus());
        assertEquals(taskEntity.getDeadline().format(FORMATTER), taskDto.getDeadline());
        assertEquals(taskEntity.getDescription(), taskDto.getDescription());
    }

    @Test
    void toDtoWithOwnerTest() {
        TaskWithOwnerDto dto = taskMapper.toDtoWithOwner(taskEntity);

        assertEquals(taskEntity.getId(), dto.getId());
        assertEquals(taskEntity.getStatus(), dto.getStatus());
        assertEquals(taskEntity.getDeadline().format(FORMATTER), dto.getDeadline());
        assertEquals(taskEntity.getDescription(), dto.getDescription());
        assertEquals(taskEntity.getOwner().getId(), dto.getOwner().getId());
        assertEquals(taskEntity.getOwner().getRole(), dto.getOwner().getRole());
        assertEquals(taskEntity.getOwner().getLogin(), dto.getOwner().getLogin());
        assertEquals(taskEntity.getOwner().getName(), dto.getOwner().getName());
    }

    @Test
    void createToEntityTest() {
        String deadline = LocalDateTime.now().format(FORMATTER);
        CreateTaskDto dto = new CreateTaskDto()
                .setDeadline(deadline)
                .setDescription("description");
        TaskEntity task = taskMapper.createToEntity(dto);

        assertEquals(0, task.getId());
        assertEquals(Status.PLANNED, task.getStatus());
        assertEquals(dto.getDescription(), task.getDescription());
        assertEquals(deadline, task.getDeadline().format(FORMATTER));
        assertNull(task.getOwner());
    }

    @Test
    void updateEntityFromCreateDtoTest() {
        String dtoDescription = "dtoDescription";
        LocalDateTime deadline = LocalDateTime.now();
        String dtoDeadline = LocalDateTime.now().plusDays(10).format(FORMATTER);
        CreateTaskDto dto = new CreateTaskDto()
                .setDeadline(dtoDeadline)
                .setDescription(dtoDescription);
        TaskEntity task = new TaskEntity()
                .setDescription("description")
                .setDeadline(deadline);
        taskMapper.updateEntityFromCreateDto(dto, task);

        assertEquals(dtoDescription, task.getDescription());
        assertEquals(dtoDeadline, task.getDeadline().format(FORMATTER));
    }
}