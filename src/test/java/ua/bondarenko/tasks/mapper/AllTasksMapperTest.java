package ua.bondarenko.tasks.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;
import ua.bondarenko.tasks.dto.*;
import ua.bondarenko.tasks.entity.TaskEntity;
import ua.bondarenko.tasks.entity.UserEntity;

import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AllTasksMapperTest {
    private final AllTasksMapper allTasksMapper = Mappers.getMapper(AllTasksMapper.class);
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
    private final TaskMapper taskMapper = Mappers.getMapper(TaskMapper.class);

    private final UserEntity userEntity1 = new UserEntity().setId(1);
    private final UserEntity userEntity2 = new UserEntity().setId(2);
    private final TaskEntity taskEntity1 = new TaskEntity().setId(1).setOwner(userEntity1);
    private final TaskEntity taskEntity2 = new TaskEntity().setId(2).setOwner(userEntity2);
    private final List<TaskEntity> content = List.of(taskEntity1, taskEntity2);
    private final int pageSize = 3;
    private final int pageNumber = 1;
    private final long totalTasks = 9;
    private final Page<TaskEntity> page = new PageImpl<>(
            content,
            Pageable.ofSize(pageSize).withPage(pageNumber),
            totalTasks);


    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(taskMapper, "userMapper", userMapper);
        ReflectionTestUtils.setField(allTasksMapper, "taskMapper", taskMapper);
    }

    @Test
    void pageToDtoTest() {
        AllTasksDto dto = allTasksMapper.pageToDto(page);
        assertEquals(pageNumber, dto.getCurrentPage());
        assertEquals(totalTasks / pageSize, dto.getNumberOfPages());

        List<TaskDto> taskDtos = dto.getTasks();
        assertEquals(content.size(), taskDtos.size());

        IntStream
                .range(0, content.size())
                .forEach(index -> assertEquals(content.get(index).getId(), taskDtos.get(index).getId()));
    }

    @Test
    void pageToDtoWithOwnersTest() {
        AllTasksWithOwnersDto dto = allTasksMapper.pageToDtoWithOwners(page);
        assertEquals(pageNumber, dto.getCurrentPage());
        assertEquals(totalTasks / pageSize, dto.getNumberOfPages());

        List<TaskWithOwnerDto> taskDtos = dto.getTasks();
        assertEquals(content.size(), taskDtos.size());

        IntStream
                .range(0, content.size())
                .forEach(index -> {
                    TaskEntity taskEntity = content.get(index);
                    TaskWithOwnerDto taskDto = taskDtos.get(index);
                    assertEquals(taskEntity.getId(), taskDto.getId());
                    assertEquals(taskEntity.getOwner().getId(), taskDto.getOwner().getId());
                });
    }
}