package ua.bondarenko.tasks.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import ua.bondarenko.tasks.dto.CreateUserDto;
import ua.bondarenko.tasks.dto.UserDto;
import ua.bondarenko.tasks.entity.UserEntity;
import ua.bondarenko.tasks.model.Role;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Mock
    private PasswordEncoder passwordEncoderMock;

    private final UserEntity userEntity = new UserEntity()
            .setId(1)
            .setName("name")
            .setLogin("login")
            .setPassword("password")
            .setRole(Role.ROLE_ADMIN);

    @BeforeEach
    void setUp() {
        userMapper.setPasswordEncoder(passwordEncoderMock);
    }

    @Test
    void toDtoTest() {
        UserDto dto = userMapper.toDto(userEntity);
        assertEquals(userEntity.getId(), dto.getId());
        assertEquals(userEntity.getName(), dto.getName());
        assertEquals(userEntity.getLogin(), dto.getLogin());
        assertEquals(userEntity.getRole(), dto.getRole());
    }

    @Test
    void createDtoToEntityTest() {
        CreateUserDto dto = new CreateUserDto()
                .setName("name")
                .setLogin("login")
                .setPassword("password");
        String encodedPassword = "encoded password";

        when(passwordEncoderMock.encode(dto.getPassword())).thenReturn(encodedPassword);

        UserEntity entity = userMapper.createDtoToEntity(dto);

        assertEquals(dto.getName(), entity.getName());
        assertEquals(dto.getLogin(), entity.getLogin());
        assertEquals(encodedPassword, entity.getPassword());
        assertEquals(Role.ROLE_USER, entity.getRole());
    }

    @Test
    void updateEntityFromCreateDto() {
        CreateUserDto dto = new CreateUserDto()
                .setName("name")
                .setLogin("login")
                .setPassword("password");
        UserEntity entity = new UserEntity()
                .setName("old name")
                .setLogin("old login")
                .setPassword("old password");
        String encodedPassword = "encoded password";
        when(passwordEncoderMock.encode(dto.getPassword())).thenReturn(encodedPassword);

        userMapper.updateEntityFromCreateDto(dto, entity);

        assertEquals(dto.getName(), entity.getName());
        assertEquals(dto.getLogin(), entity.getLogin());
        assertEquals(encodedPassword, entity.getPassword());
    }
}